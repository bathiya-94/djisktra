# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <ctime>
# include <omp.h>

using namespace std;

# define NO_OF_VERTICES 6

int main(int argc, char **argv);
int *dijsktra_distance(int graph[NO_OF_VERTICES][NO_OF_VERTICES]);

void find_nearest(int starting_node, int ending_node, int mind[NO_OF_VERTICES], bool connencted[NO_OF_VERTICES],
                    int *d, int *v);
void init(int graph[NO_OF_VERTICES][NO_OF_VERTICES]);
void update_mind(int starting_node, int ending_node, int mv ,
                 bool connected[NO_OF_VERTICES] ,int graph[NO_OF_VERTICES][NO_OF_VERTICES]);
void print_graph(int graph[NO_OF_VERTICES][NO_OF_VERTICES]);                 

int main ( int argc, char **argv)
{
    int i,j;
    int *mind;
    int graph[NO_OF_VERTICES][NO_OF_VERTICES];

    

    printf("Dijsktra Parallel OpenMp by Bathiya\n");
    printf("\n");

    init(graph); // Initializes the graph

    printf("\n");
    printf("Graph's Distance Matrix");
    printf("\n");

    print_graph(graph);

    for (int i = 0; i < NO_OF_VERTICES; i++)
    {
        for (int j =0; j <NO_OF_VERTICES; j++)
        {
            if(graph[i][j] == INT_MAX)
            {
                printf("  Inf");
            }

            else 
            {
                cout<< "  "<<setw(3)<< graph[i][j];
            }
        }
        printf("\n");
    }

    mind = dijsktra_distance(graph);

    printf("\n");
    printf("Minimum distances from node 0:\n");
    printf("\n");

     for ( i = 0; i < NO_OF_VERTICES; i++ )
     {
        cout << "  " << setw(2) << i
         << "  " << setw(2) << mind[i] << "\n";
     }

    delete [] mind;

    printf("\n");
    printf("Dijsktra OPENMP : End of Exec\n");
    printf("\n");


    return 0;
}

void init(int graph[NO_OF_VERTICES][NO_OF_VERTICES])
{
    for (int i = 0; i <NO_OF_VERTICES; i++)
    {
        for(int j=0; j< NO_OF_VERTICES;j++)
        {
            if(i == j)
            {
                graph[i][i] =0 ;
            }
                
            else
            {
                graph[i][j] = INT_MAX ;
            }
        }
    }

    graph[0][1] = graph[1][0] = 40;
    graph[0][2] = graph[2][0] = 15;
    graph[1][2] = graph[2][1] = 20;
    graph[1][3] = graph[3][1] = 10;
    graph[1][4] = graph[4][1] = 25;
    graph[2][3] = graph[3][2] = 100;
    graph[1][5] = graph[5][1] = 6;
    graph[4][5] = graph[5][4] = 8;
}

void update_mind(int starting_node, int ending_node , int mv, bool connect[NO_OF_VERTICES], 
                int graph[NO_OF_VERTICES][NO_OF_VERTICES] , int mind[NO_OF_VERTICES])
{
    for (int i = starting_node ; i<=ending_node; i++)
    {
        if(!connect[i])
        {
            if (graph[mv][i] < INT_MAX)
            {
                 if(mind[mv]+graph[mv][i] < mind[i])
                 {
                     mind[i] = mind[mv] +graph[mv][i];
                 }
            }
        }
    }

    return;
}

void timestamp()
{

    # define TIME_SIZE 40

    static char time_buffer[TIME_SIZE];
    const struct  std::tm *tm_ptr;
    size_t len;
    std::time_t now;

    now = std::time(NULL);
    tm_ptr = std::localtime(&now);

    len = std::strftime ( time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm_ptr );

    std::cout << time_buffer << "\n";

    return;
    # undef TIME_SIZE
}


int *dijsktra_distance (int graph[NO_OF_VERTICES][NO_OF_VERTICES])
{
    bool *connected;
    int md;
    int *mind;
    int mv ,first,last,my_id,my_md,my_mv,my_step,nth;

    connected = new bool[NO_OF_VERTICES];
    connected[0] = true;

    for (int i = 1; i < NO_OF_VERTICES;i++ )
    {
        connected[i] = false;
    }
//
    for (int i = 0; i < NO_OF_VERTICES ; i++)
    {
         mind[i] = graph[0][i];
    }

    // Begin parallel region

    #pragma omp parallel private(first,my_id,last,my_md, my_mv, my_step)\
                         shared(connected, md, mind, mv , nth, graph)
    {
        my_id = omp_get_thread_num();
        nth = omp_get_num_threads();

        first = (my_id * NO_OF_VERTICES) / nth;
        last = ((my_id +1) * NO_OF_VERTICES) / nth -1;

        # pragma omp single
        {
            printf("\n");
            printf(" P %d Parallel Region begins with %d threads.\n");
            printf("\n");

        }

        printf("P %d First=%d Last= %d \n", my_id, first, last);

        for (my_step =1; my_step<NO_OF_VERTICES; my_step++)
        {
            # pragma omp single
            {
                md = INT_MAX;
                mv = -1;
            }
        

        find_nearest(first, last,mind,connected, &my_md, &my_mv);

        # pragma omp critical
        {
            md = my_md;
            mv = my_mv;
        }

        #pragma omp parallel

        # pragma omp single 
        {
            if(mv!=-1)
            {
                connected[mv] = true;
                printf("P %d :Connecting node %d\n", my_id,mv);

            }
        }

            # pragma omp barrier
        }
        if ( mv != -1 )
        {
            update_mind ( first, last, mv, connected, graph, mind );
        }
        
        #pragma omp barrier

         # pragma omp single
        {
            printf("\n");
            printf("P %d .Exiting Parallel region.\n");
        
        }
    }

    delete [] connected;
    return mind;    
}

void find_nearest(int starting_node, int ending_node, int mind[NO_OF_VERTICES],bool connected[NO_OF_VERTICES],
                int *d, int *v)
{
    *d = INT_MAX;
    *v = -1;

    for(int i =starting_node; i<=ending_node; i++)
    {
        if ( !connected[i] && mind[i] < *d )
        {
            *d = mind[i];
            *v = i;
        }
    }
}

void print_graph(int graph[NO_OF_VERTICES][NO_OF_VERTICES])
{
    for (int i = 0; i < NO_OF_VERTICES; i++)
    {
        for (int j =0; j <NO_OF_VERTICES; j++)
        {
            if(graph[i][j] == INT_MAX)
            {
                printf("  Inf");
            }

            else 
            {
                cout<< "  "<<setw(3)<< graph[i][j];
            }
        }
        printf("\n");
    }
}