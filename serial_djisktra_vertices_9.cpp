# include <limits.h>
# include <stdio.h>
# include <time.h>
# include <iostream>
# include <cstdlib>
# include <iomanip>

using namespace std;

const int  NO_OF_VERTICES =9;

int miniumDistance(int distance[], bool isVisited[]);
int printSolution(int distance[]);
void djisktra(int graph[NO_OF_VERTICES][NO_OF_VERTICES], int source);
void print_graph(int graph[NO_OF_VERTICES][NO_OF_VERTICES]);

int main () 
{
     
    int graph[NO_OF_VERTICES] [NO_OF_VERTICES] ={ { 0, 4, 0, 0, 0, 0, 0, 8, 0 }, 
                        { 4, 0, 8, 0, 0, 0, 0, 11, 0 }, 
                        { 0, 8, 0, 7, 0, 4, 0, 0, 2 }, 
                        { 0, 0, 7, 0, 9, 14, 0, 0, 0 }, 
                        { 0, 0, 0, 9, 0, 10, 0, 0, 0 }, 
                        { 0, 0, 4, 14, 10, 0, 2, 0, 0 }, 
                        { 0, 0, 0, 0, 0, 2, 0, 1, 6 }, 
                        { 8, 11, 0, 0, 0, 0, 1, 0, 7 }, 
                        { 0, 0, 2, 0, 0, 0, 6, 7, 0 } };

    printf("Printing the Graph\n");
    print_graph(graph);
    printf("\n");
    printf("Minimum Distances");
                        
    clock_t tStart = clock();
    djisktra(graph,0);
    printf("The program executed in: %.2fms\n", ((double)(clock() - tStart)/CLOCKS_PER_SEC)*1000);
    return 0;
}

// Print the array
int printSolution(int distance[]){
    printf("\n");
    printf("Vertex \t\t Distance from Source\n");
    for (int i =0 ; i<NO_OF_VERTICES;i++)
    {
        printf("%d \t \t %d \n" , i, distance[i]);
    }
}

//Find the minimum distance

int miniumDistance(int distance[], bool isVisited[]){
    
    int min = INT_MAX, min_index;

    for (int i=0; i <NO_OF_VERTICES; i++){
        if (isVisited[i] ==false && distance[i] <=min)
            { 
                min = distance[i] ;
                min_index = i;
            }
    }

    return min_index;
}

void djisktra(int graph[NO_OF_VERTICES][NO_OF_VERTICES], int source){
    
    int distance[NO_OF_VERTICES] ; // Will hold  the shortest distances
    bool isVisited[NO_OF_VERTICES]; // Will hold  true if  the vertex i is included in shortest path tree

    // Initialize all distances as INFINITE and stpSet as false 
    for(int i =0; i<NO_OF_VERTICES ; i++){
        distance[i] = INT_MAX;
        isVisited[i] = false;
    }

    distance[source] = 0; // Distance from source to itself is zero

    // Finding the shortest path to  all vertices from the source
    for (int i =0; i<NO_OF_VERTICES; i++){
        // The minimujm distance vertex from the non_visited  vertexes is selected
        int u  = miniumDistance(distance, isVisited);

        isVisited[u] = true;// Set the visited the nodes as true


        //  Update distance value of the adjacent vertices of the picked vertex
        for (int i=0; i<NO_OF_VERTICES ;i++){
            /*
                Distance is updateed  if all following conditions are satisified
                    1.only if is not in sptSet,
                    2. If there is an edge from u to v
                    3. If the distance  is smaller than the current value 

            */

           if (!isVisited[i] && graph[u][i] && distance[u] != INT_MAX
            && distance[u] + graph[u][i] < distance[i])
           {
               distance[i] = distance[u] + graph [u][i] ;
           }       
        }       
    }
    printSolution(distance);
}

void print_graph(int graph[NO_OF_VERTICES][NO_OF_VERTICES])
{
    for (int i = 0; i < NO_OF_VERTICES; i++)
    {
        for (int j =0; j <NO_OF_VERTICES; j++)
        {
            if(graph[i][j] == INT_MAX)
            {
                printf("  Inf");
            }

            else 
            {
                cout<< "  "<<setw(3)<< graph[i][j];
            }
        }
        printf("\n");
    }
}