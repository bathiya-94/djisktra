# include <limits.h>
# include <stdio.h>
# include <time.h>
# include <iostream>
# include <iomanip>

using namespace std;
const int  NO_OF_VERTICES = 6;


int miniumDistance(int distance[], bool isVisited[]);
int printSolution(int distance[]);
void djisktra(int graph[NO_OF_VERTICES][NO_OF_VERTICES], int source);
void init(int graph[NO_OF_VERTICES][NO_OF_VERTICES]);
void print_graph(int graph[NO_OF_VERTICES][NO_OF_VERTICES]);


int main () 
{

    int graph[NO_OF_VERTICES][NO_OF_VERTICES];
    init(graph);

    printf("Printing the Graph\n");
    print_graph(graph);
    printf("\n");
    
    clock_t tStart = clock(); // Initialize clock

    djisktra(graph,0);
    printf("\nThe program executed in: %.2fms\n", ((double)(clock() - tStart)/CLOCKS_PER_SEC)*1000);
    return 0;
}




// Print the array
int printSolution(int distance[]){
      printf("Vertex \t\t Distance from Source\n");
      for (int i =0 ; i<NO_OF_VERTICES;i++){
          printf("%d \t \t %d \n" , i, distance[i]);
      }
}

//Find the minimum distance
int miniumDistance(int distance[], bool isVisited[]){
    
    int min = INT_MAX, min_index;

    for (int i=0; i <NO_OF_VERTICES; i++){
        if (isVisited[i] ==false && distance[i] <=min)
            { 
                min = distance[i] ;
                min_index = i;
            }
    }

    return min_index;
}

void djisktra(int graph[NO_OF_VERTICES][NO_OF_VERTICES], int source){
    
    int distance[NO_OF_VERTICES] ; // Will hold  the shortest distances
    bool isVisited[NO_OF_VERTICES]; // Will hold  true if  the vertex i is included in shortest path tree

    // Initialize all distances as INFINITE and Visited as false 
    for(int i =0; i<NO_OF_VERTICES; i++){
        distance[i] = INT_MAX;
        isVisited[i] = false;
    }

    distance[source] = 0; // Distance from source to itself is zero

    // Finding the shortest path to  all vertices from the source
    for (int i =0; i<NO_OF_VERTICES-1; i++){
        // The minimujm distance vertex from the non_visited  vertexes is selected
        int u  = miniumDistance(distance, isVisited);

        isVisited[u] = true;// Set the visited the nodes as true


        //  Update distance value of the adjacent vertices of the picked vertex
        for (int j=0; j<NO_OF_VERTICES ;j++){
            /*
                Distance is updateed  if all following conditions are satisified
                    1.only if is not in sptSet,
                    2. If there is an edge from u to v
                    3. If the distance  is smaller than the current value 

            */

           if (!isVisited[j] && graph[u][j] !=INT_MAX && distance[u] != INT_MAX
            && distance[u] + graph[u][j] < distance[j])
           {
               distance[j] = distance[u] + graph [u][j] ; // Checked with other vertex in the graph

           }       
        }       
    }
    printSolution(distance);
}


void init(int graph[NO_OF_VERTICES][NO_OF_VERTICES])
{
    for (int i = 0; i <NO_OF_VERTICES; i++)
    {
        for(int j=0; j< NO_OF_VERTICES;j++)
        {
            if(i == j)
            {
                graph[i][i] =0 ;
            }
                
            else
            {
                graph[i][j] = INT_MAX ;
            }
        }
    }

    // Initalize the distances
    graph[0][1] = graph[1][0] = 40;
    graph[0][2] = graph[2][0] = 15;
    graph[1][2] = graph[2][1] = 20;
    graph[1][3] = graph[3][1] = 10;
    graph[1][4] = graph[4][1] = 25;
    graph[2][3] = graph[3][2] = 100;
    graph[1][5] = graph[5][1] = 6;
    graph[4][5] = graph[5][4] = 8;
}


void print_graph(int graph[NO_OF_VERTICES][NO_OF_VERTICES])
{
    // print the graph
    for (int i = 0; i < NO_OF_VERTICES; i++)
    {
        for (int j =0; j <NO_OF_VERTICES; j++)
        {
            if(graph[i][j] == INT_MAX)
            {
                printf("  Inf");
            }
            else 
            {
                cout<< "  "<<setw(3)<< graph[i][j];         
            }
        }
        printf("\n");
    }
}